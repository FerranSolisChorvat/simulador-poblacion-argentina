import { escenario } from "./escenarios.js";
import { simularEscenario } from "./ecuaciones.js";

//1: 2020, ..., 81: 2100
export function simular(tipo) {
  return { poblacion: simularEscenario(81, escenario[tipo]), variables: escenario[tipo] };
}

export function simularPersonalizado(natalidad, holguraN, mortalidad, holguraM, inmigracion, emigracion) {
  const escenarioP = {
    natalidadPeriodo: natalidad.map((natP) => {
      return [natP - holguraN < 0 ? 0 : (natP - holguraN) / 1000, (natP + holguraN) / 1000];
    }),
    mortalidadPeriodo: mortalidad.map((morP) => {
      return [morP - holguraM < 0 ? 0 : (morP - holguraM) / 1000, (morP + holguraM) / 1000];
    }),
    tasa_inmigracion_inicial: inmigracion / 1000,
    tasa_emigracion_inicial: emigracion / 1000,
  };
  console.log(escenarioP);
  return { poblacion: simularEscenario(81, escenarioP), variables: escenarioP };
}
