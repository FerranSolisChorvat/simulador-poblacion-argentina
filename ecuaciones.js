import generarMu from "./generarU.js";

const población_inicial = 45376763;

export function simularEscenario(k, { natalidadPeriodo, mortalidadPeriodo, tasa_inmigracion_inicial, tasa_emigracion_inicial }) {
  const poblacionTotal = ["Inicio"];

  return poblacion(k);

  function poblacion(k) {
    poblacionTotal.push(población_inicial);
    for (let K = 2; K <= k; K++) {
      const J = K - 1;
      const L = K + 1;
      const poblacion_anio = poblacionTotal[J] + nacimientos(K, L) + inmigracion(K, L) - defunciones(K, L) - emigracion(K, L);
      poblacionTotal.push(Math.round(poblacion_anio));
    }
    poblacionTotal.shift();
    return poblacionTotal;
  }

  function nacimientos(K, L) {
    const J = K - 1;
    return poblacionTotal[J] * natalidad(K);
  }
  function defunciones(K, L) {
    const J = K - 1;
    return poblacionTotal[J] * mortalidad(K);
  }
  function inmigracion(K, L) {
    const J = K - 1;
    return poblacionTotal[J] * tasa_inmigracion_inicial;
  }
  function emigracion(K, L) {
    const J = K - 1;
    return poblacionTotal[J] * tasa_emigracion_inicial;
  }
  function natalidad(k) {
    const K = Math.ceil(k / 10);
    const u = generarMu();
    const [min, max] = natalidadPeriodo[K - 1];
    const maxValue = Math.max(min, max);
    const minValue = Math.min(min, max);
    return minValue + (maxValue - minValue) * u;
  }
  function mortalidad(k) {
    const K = Math.ceil(k / 10);
    const u = generarMu();
    const [min, max] = mortalidadPeriodo[K - 1];
    const maxValue = Math.max(min, max);
    const minValue = Math.min(min, max);
    return minValue + (maxValue - minValue) * u;
  }
}
