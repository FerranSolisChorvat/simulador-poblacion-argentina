//Genero mi variable pseudoaleatoria usando el método Congruencial Aditivo
var x0 = new Date().getSeconds();
var x1 = new Date().getMilliseconds();
var x2 = new Date().getMinutes();
var k = 3;
var m = 1000;

export default function generarMu() {
  var u = (x0 + x2) % m;
  x0 = x1;
  x1 = x2;
  x2 = u;
  return Number((u * 0.001).toFixed(k));
}
