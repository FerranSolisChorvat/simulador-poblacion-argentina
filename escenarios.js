export const escenario = {
  normal: {
    natalidadPeriodo: [
      [0.0139, 0.0139],
      [0.0148, 0.0151],
      [0.014, 0.0135],
      [0.01243, 0.01243],
      [0.0115, 0.011],
      [0.0109, 0.0105],
      [0.0098, 0.0095],
      [0.0093, 0.009],
      [0.00918, 0.00918],
    ],
    mortalidadPeriodo: [
      [0.009, 0.009],
      [0.0079, 0.0081],
      [0.0089, 0.0091],
      [0.00865, 0.00865],
      [0.0111, 0.0113],
      [0.0118, 0.012],
      [0.0123, 0.0124],
      [0.0124, 0.0126],
      [0.01252, 0.01252],
    ],
    tasa_inmigracion_inicial: 0.00086,
    tasa_emigracion_inicial: 0.00058,
  },
  pandemia: {
    natalidadPeriodo: [
      [0.0139, 0.0139], //2020
      [0.0148, 0.0151], //2030
      [0.014, 0.0135], //2040
      [0.01243, 0.01243], //2050
      [0.0115, 0.011], //2060
      [0.0109, 0.0105], //2070
      [0.0098, 0.0095], //2080
      [0.0093, 0.009], //2090
      [0.00918, 0.00918], //2100
    ],
    mortalidadPeriodo: [
      [0.009, 0.009], //2020
      [0.0079, 0.0081], //2030
      [0.0089, 0.0091], //2040
      [0.01152, 0.01152], //2050 (impacto de la pandemia)
      [0.0139, 0.0147], //2060 (recuperación post-pandemia)
      [0.0118, 0.012], //2070
      [0.0123, 0.0124], //2080
      [0.0124, 0.0126], //2090
      [0.01252, 0.01252], //2100
    ],
    tasa_inmigracion_inicial: 0.00086,
    tasa_emigracion_inicial: 0.00058,
  },
  guerra: {
    natalidadPeriodo: [
      [0.0136, 0.0142], //2020
      [0.0099, 0.0102], //2030 (Impacto de la guerra)
      [0.014, 0.0135], //2040 (Recuperación post-guerra)
      [0.01243, 0.01243], //2050
      [0.0115, 0.011], //2060
      [0.0109, 0.0105], //2070
      [0.0098, 0.0095], //2080
      [0.0093, 0.009], //2090
      [0.00918, 0.00918], //2100
    ],
    mortalidadPeriodo: [
      [0.009, 0.011], //2020
      [0.0128, 0.013], //2030 (Impacto de la guerra)
      [0.0089, 0.0091], //2040 (Recuperación post-guerra)
      [0.01152, 0.01152], //2050
      [0.0139, 0.0147], //2060
      [0.0118, 0.012], //2070
      [0.0123, 0.0124], //2080
      [0.0124, 0.0126], //2090
      [0.01252, 0.01252], //2100
    ],
    tasa_inmigracion_inicial: 0.00086,
    tasa_emigracion_inicial: 0.00058,
  },
  hiperinflacion: {
    natalidadPeriodo: [
      [0.0139, 0.0139], //2020
      [0.0148, 0.0151], //2030
      [0.014, 0.0135], //2040
      [0.01243, 0.01243], //2050
      [0.0115, 0.011], //2060
      [0.0109, 0.0105], //2070
      [0.0098, 0.0095], //2080
      [0.0093, 0.009], //2090
      [0.00918, 0.00918], //2100
    ],
    mortalidadPeriodo: [
      [0.009, 0.009], //2020
      [0.0079, 0.0081], //2030
      [0.0089, 0.0091], //2040
      [0.01152, 0.01152], //2050
      [0.0139, 0.0147], //2060
      [0.0118, 0.012], //2070
      [0.0123, 0.0124], //2080
      [0.0124, 0.0126], //2090
      [0.01252, 0.01252], //2100
    ],
    tasa_inmigracion_inicial: 0.00006,
    tasa_emigracion_inicial: 0.00358,
  },
  mejora_economica: {
    natalidadPeriodo: [
      [0.0139, 0.0139], //2020
      [0.0148, 0.0151], //2030
      [0.0149, 0.0152], //2040
      [0.01501, 0.01506], //2050
      [0.01523, 0.01528], //2060
      [0.0155, 0.0158], //2070
      [0.01593, 0.016], //2080
      [0.01623, 0.0163], //2090
      [0.0164, 0.0168], //2100
    ],
    mortalidadPeriodo: [
      [0.009, 0.009], //2020
      [0.0079, 0.0081], //2030
      [0.0089, 0.0091], //2040
      [0.01152, 0.01152], //2050
      [0.0139, 0.0147], //2060
      [0.0118, 0.012], //2070
      [0.0123, 0.0124], //2080
      [0.0124, 0.0126], //2090
      [0.01252, 0.01252], //2100
    ],
    tasa_inmigracion_inicial: 0.00486,
    tasa_emigracion_inicial: 0.00028,
  },
};

// Caso pandemia
// Adicionamos un porcentaje de mortalidad que se extrajo del total
// de muertes por covid en la última pandemia para tener referencia
// Porcentaje extra de mortalidad debido a la pandemia 0,002875
// Porcentaje extra de mortalidad debido a la guerra

// Caso Hiperinflación
// Utilizamos como ejemplo los números de venezuela
// Población Venezuela 26.577.423
// Emigrantes 5.415.337

// Caso Guerra
// Suponemos un número grande de muertes y sacamos una tasa en función de la población y el numero de muertes
// Adicionamos esta tasa a la tasa de mortalidad normal, en el año de impacto y de post impacto
// Suponemos un número de bajas de 225.000
// Porcentaje extra de mortalidad debido a la guerra 0,004945

// Caso Deflación
// Caso Hiperinflación
